package com.marsol0x.todotxt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.Collections;

public class App
{
    private static ArrayList<TodoItem> todoItems = new ArrayList<>();
    private static ArrayList<TodoItem> doneItems = new ArrayList<>();

    public static void main(String[] args)
    {
        File todoFile = null;
        String todoFilename = "todo.txt";
        String pathSeparator = File.separator;
        
        // NOTE: Find the todo.txt file to use
        String[] possiblePaths = { System.getProperty("user.dir") + pathSeparator + todoFilename,
                                   System.getProperty("user.home") + pathSeparator + todoFilename };
        for (String path : possiblePaths)
        {
            File f = new File(path);
            if (f.isFile())
            {
                todoFile = f;
                break;
            }
        }

        if (todoFile == null)
        {
            System.err.println("Could not find todo.txt");
            System.exit(1);
        }

        // TODO: test read/write permissions
        parse(todoFile, todoItems, doneItems);

        if (args.length == 0)
        {
            Commands.list(todoItems);
        } else {
            switch (args[0].toLowerCase())
            {
                case "list":
                case "ls":
                    {
                        Commands.list(todoItems);
                    }
                    break;

                case "add":
                case "a":
                    {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 1; i < args.length; i++)
                        {
                            sb.append(args[i]);
                            if (i + 1 != args.length)
                            {
                                sb.append(" ");
                            }
                        }
                        Commands.add(todoItems, sb.toString());
                        write(todoFile, todoItems, doneItems);
                    }
                    break;

                case "done":
                case "do":
                case "d":
                    {
                        if (args.length < 2)
                        {
                            System.out.println("Must provide an index number to mark items as done.");
                            break;
                        }
                        TodoItem item = Commands.done(todoItems, doneItems, Integer.parseInt(args[1]));
                        System.out.println("Completed: " + item.getItem());
                        write(todoFile, todoItems, doneItems);
                    }
                    break;

                case "priority":
                case "pri":
                case "p":
                    {
                        if (args.length < 2)
                        {
                            System.out.println("Must provide an index number to increase priority.");
                            break;
                        }
                        Commands.priority(todoItems, Integer.parseInt(args[1]));
                        write(todoFile, todoItems, doneItems);
                    }
                    break;

                case "depriority":
                case "depri":
                case "dp":
                    {
                        if (args.length < 2)
                        {
                            System.out.println("Must provide an index number to decrease priority.");
                            break;
                        }
                        Commands.depriority(todoItems, Integer.parseInt(args[1]));
                        write(todoFile, todoItems, doneItems);
                    }
                    break;

                case "delete":
                case "del":
                    {
                        if (args.length < 2)
                        {
                            System.out.println("Must provide an index number to delete.");
                            break;
                        }
                        int itemNum = Integer.parseInt(args[1]);
                        TodoItem item = todoItems.get(itemNum - 1);
                        System.out.println("Deleted: " + item.getItem());
                        Commands.delete(todoItems, itemNum);
                        write(todoFile, todoItems, doneItems);
                    }
                    break;

                case "num":
                {
                    System.out.println(todoItems.size());
                }
                break;

                default:
                    System.out.println("HALP!\nLook at the source for now.");
                    break;
            }
        }
    }

    public static void parse(File file, ArrayList<TodoItem> todoItems, ArrayList<TodoItem> doneItems)
    {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file)))
        {

            // NOTE: Grab each line in the todo file and create the todoItems
            // list
            ArrayList<TodoItem> items = todoItems;
            while (bufferedReader.ready())
            {
                String line = bufferedReader.readLine();
                if (line.equals("-"))
                {
                    items = doneItems;
                    continue;
                }

                // NOTE: A single TodoItem line looks like this:
                // Date     P Todo
                // 20150512 A Organize JIRA tickets by priority @work
                //  Date: Create date
                //  P   : Priority
                //  Todo: The todo item
                String[] parts = line.split(" ", 3);
                String createDate = parts[0];
                int priority = parts[1].charAt(0);
                String item = parts[2];

                items.add(new TodoItem(createDate, priority, item));
            }
        } catch (FileNotFoundException e) {
            // NOTE: This should not occur because we check for it before this
            // method. If it does happen we need to break hard and fast
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            // NOTE: This prints and dies because that's all I need right now
            e.printStackTrace();
            System.exit(2);
        }
    }

    public static void write(File file, ArrayList<TodoItem> todoItems, ArrayList<TodoItem> doneItems)
    {
        Collections.sort(todoItems);
        Collections.sort(doneItems);
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file))))
        {
            for (TodoItem item : todoItems)
            {
                writer.println(item);
            }
            writer.println("-");
            for (TodoItem item : doneItems)
            {
                writer.println(item);
            }
        } catch (FileNotFoundException e) {
            // NOTE: This should not occur because we check for it before this
            // method. If it does happen we need to break hard and fast
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            // NOTE: This prints and dies because that's all I need right now
            e.printStackTrace();
            System.exit(2);
        }
    }
}
