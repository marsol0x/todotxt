package com.marsol0x.todotxt;

import java.util.ArrayList;
import java.util.Collections;

public class Commands
{
    public static void list(ArrayList<TodoItem> items)
    {
        if (items.size() == 0)
        {
            System.out.println("todo.txt is empty");
            return;
        } else if (items.size() > 1) {
            System.out.println(String.format("%d Todo items", items.size()));
        } else {
            System.out.println(String.format("%d Todo item", items.size()));
        }
        System.out.println("---");

        int i = 1;
        Collections.sort(items);
        for (TodoItem item : items)
        {
            System.out.println(String.format("%2d", i) + " " + item);
            i++;
        }
    }

    public static void add(ArrayList<TodoItem> items, String str)
    {
        items.add(new TodoItem(str));
    }

    public static TodoItem done(ArrayList<TodoItem> todoItems, ArrayList<TodoItem> doneItems, int index)
    {
        TodoItem item = todoItems.get(index - 1);
        todoItems.remove(item);
        doneItems.add(item);

        return item;
    }

    public static void priority(ArrayList<TodoItem> todoItems, int index)
    {
        TodoItem item = todoItems.get(index - 1);
        item.setPriority(item.getPriority() - 1);
    }

    public static void depriority(ArrayList<TodoItem> todoItems, int index)
    {
        TodoItem item = todoItems.get(index - 1);
        item.setPriority(item.getPriority() + 1);
    }

    public static void delete(ArrayList<TodoItem> todoItems, int index)
    {
        TodoItem item = todoItems.get(index - 1);
        todoItems.remove(item);
    }
}
