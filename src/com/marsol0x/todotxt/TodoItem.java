package com.marsol0x.todotxt;

import java.lang.Comparable;
import java.lang.StringBuilder;
import java.util.Calendar;
import java.util.HashMap;

public class TodoItem implements Comparable<TodoItem>
{
    private static HashMap<Integer, Character> priorityMap = new HashMap<>(6);

    static
    {
        priorityMap.put(0, 'A');
        priorityMap.put(1, 'B');
        priorityMap.put(2, 'C');
        priorityMap.put(3, 'D');
        priorityMap.put(4, 'E');
        priorityMap.put(5, 'F');
    }

    private Calendar createDate;
    private int priority;
    private String item;

    public TodoItem(String date, int priority, String item)
    {
        this.createDate = Calendar.getInstance();
        this.item = item;
        this.setPriority(priority);


        if (date != null)
        {
            String yearString = date.substring(0, 4);
            String monthString = date.substring(4, 6);
            String dayString = date.substring(6, 8);
            String hourString = date.substring(8, 10);
            String minuteString = date.substring(10);

            createDate.set(Integer.valueOf(yearString),
                           Integer.valueOf(monthString) - 1, // January == 0
                           Integer.valueOf(dayString),
                           Integer.valueOf(hourString),
                           Integer.valueOf(minuteString),
                           0);
        }
    }

    public TodoItem(int priority, String item)
    {
        this(null , priority, item);
    }

    public TodoItem(String item)
    {
        this(null, 5, item);
    }

    public final Calendar getCreateDate()
    {
        return createDate;
    }

    public String getItem()
    {
        return item;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        if (Character.isLetter(priority))
        {
            priority = Character.toUpperCase(priority);
            priority -= (int) 'A';
        }

        if (priority > 5)
        {
            priority = 5;
        } else if (priority < 0) {
            priority = 0;
        }

        this.priority = priority;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(createDate.get(Calendar.YEAR));
        sb.append(String.format("%02d", createDate.get(Calendar.MONTH) + 1)); // January == 0
        sb.append(String.format("%02d", createDate.get(Calendar.DAY_OF_MONTH)));
        sb.append(String.format("%02d", createDate.get(Calendar.HOUR_OF_DAY)));
        sb.append(String.format("%02d", createDate.get(Calendar.MINUTE)));
        sb.append(" ");
        sb.append(priorityMap.get(priority));
        sb.append(" ");
        sb.append(item);

        return sb.toString();
    }

    @Override
    public int compareTo(TodoItem o)
    {
        final int before = -1;
        final int equal = 0;
        final int after = 1;

        if (this == o) return equal;

        if (this.priority > o.getPriority()) return after;
        if (this.priority < o.getPriority()) return before;

        int comparison = this.createDate.compareTo(o.getCreateDate());
        if (comparison != equal) return comparison;

        return this.item.compareToIgnoreCase(o.getItem());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof TodoItem)) return false;

        TodoItem item = (TodoItem) o;
        return this.createDate.equals(item.getCreateDate())
            && (this.item == item.getItem())
            && (this.priority == item.getPriority());
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash *= 13 + createDate.hashCode();
        hash *= 23 + priority;
        hash *= 7 + item.hashCode();

        return hash;
    }
}
